const d3 = require("d3");

d3.csv("Rhino_Poaching.csv").then(data => plot.call(chart, {
    data
}))

const margin = {
    top: 140,
    bottom: 80,
    left: 80,
    right: 40
};

const w = 800;
const h = 600;

const width = w - (margin.left + margin.right);
const height = h - (margin.top + margin.bottom);

const svg = d3.select("body")
    .append("svg")
    .attr("width", w)
    .attr("height", h)
    .classed("svg", true);

const chart = svg.append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`)
    .classed("display", true);

function plot(params) {
    const data = params.data;
    const xScale = d3.scaleBand()
        .domain(data.map(d => d.Year))
        .range([0, width])

    const yScale = d3.scaleLinear()
        .domain(d3.extent(data, d => +d["Rhinos poached"]))
        .range([0, height])


    const xAxis = d3.axisTop()
        .scale(xScale)
        .tickFormat(d3.format(""))
        .tickSize(10)

    const yAxis = d3.axisLeft()
        .scale(yScale)
        .tickSize(10)

    function drawGraph(params) {
        /* enter() */

        //x-axis
        chart.append("g")
            .classed("axis x-axis", true)
            .call(xAxis)

        chart.select(".x-axis")
            .append("text")
            .classed("x-label label", true)
            .text("Year")
            .attr("transform", `translate(${width / 2.1},-40)`)
            .style("font-size", 15)
            .style("fill", "black")

        // graph heading
        chart.select(".x-axis")
            .append("text")
            .classed("heading", true)
            .text("Rhinos Death Count")
            .attr("transform", `translate(${width / 2.1},-80)`)
            .style("font-size", 35)
            .style("fill", "grey")


        //y-axis
        chart.append("g")
            .classed("axis y-axis", true)
            .call(yAxis)

        chart.select(".y-axis")
            .append("text")
            .classed("y-label label", true)
            .text("Rhinos Killed")
            .attr("transform", `translate(-50,${height / 2.5}) rotate(-90)`)
            .style("font-size", 15)
            .style("fill", "black")

        // bar-container
        chart.selectAll(".bar-container")
            .data(data)
            .enter()
            .append("g")
            .classed("bar-container", true)
            .on("mouseover", function () {
                const selected = this;
                d3.selectAll(".bar-container").transition().style("opacity", function () {
                    return (this === selected) ? 1 : .2;
                })
            })
            .on("mouseout", function () {
                d3.selectAll(".bar-container").transition().duration(500).style("opacity", 1)
            })

        // bars
        chart.selectAll(".bar-container")
            .append("path")
            .classed("bar", true)


        // text
        chart.selectAll(".bar-container")
            .append("text")
            .classed("bar-text", true)

        /* update */
        chart.selectAll(".bar-container")
            .attr("transform", d => `translate(${xScale(d.Year)},0)`)

        //text
        chart.selectAll(".bar-text")
            .attr("x", xScale.bandwidth() / 2)
            .attr("y", d => yScale(d["Rhinos poached"]) + 15)
            .text(d => d["Rhinos poached"])
            .style("text-anchor", "middle")
        // .style("opacity",.5)

        // bars
        chart.selectAll(".bar")
            .attr("d", d => rectangle(0, 0, xScale.bandwidth() - 1, yScale(d["Rhinos poached"]), 0, 0, 35, 35))
            .style("fill", d => {
                if (d["Rhinos poached"] != 13)
                    return "#C33230";
                return "#F5F2EB";
            })
        //exit()
        chart.selectAll(".bar")
            .data(data)
            .exit()
            .remove();

    }

    drawGraph.call(chart, {
        data
    })

}

function concat(x, y) {
    return x + " " + y + " ";
}

function rectangle(x, y, weight, height, upperLeft, upperRight, lowerRight, lowerLeft) {
    let strPath = "M" + concat(x + upperLeft, y); //A
    strPath += "L" + concat(x + weight - upperRight, y) + "Q" + concat(x + weight, y) + concat(x + weight, y + upperRight); //B
    strPath += "L" + concat(x + weight, y + height - lowerRight) + "Q" + concat(x + weight, y + height) + concat(x + weight - lowerRight, y + height); //C
    strPath += "L" + concat(x + lowerLeft, y + height) + "Q" + concat(x, y + height) + concat(x, y + height - lowerLeft); //D
    strPath += "L" + concat(x, y + upperLeft) + "Q" + concat(x, y) + concat(x + upperLeft, y); //A
    strPath += "Z";

    return strPath;
}